/**
 * Importantly!
 * Uncomment code to see all features
 */

// Create printing machine
var printer = new PrintingMachine('MP210-series');


// Create & Add cartridges to printing machine
var bk = new Cartridge('BK40');
bk.refillInk(50);
printer.addCartridge(bk);

var pk = new Cartridge('PK41');
pk.refillInk(50);
printer.addCartridge(pk);

var yw = new Cartridge('YW42');
yw.refillInk(50);
printer.addCartridge(yw);

var be = new Cartridge('BE43');
be.refillInk(50);
printer.addCartridge(be);


// Print data about inks
printer.getCartridges();
printer.checkInkStatus();


// Create document for printing
var w3c = 'World Wide Web Consortium. The W3C is the international standards organization. Founder: Tim Berners-Lee. Founded: October 1994. CEO: Jeffrey M Jaffe. Director: Tim Berners-Lee. Staff: 62.';
var apple = 'Apple Inc. Stock price: AAPL (NASDAQ) $112.34 -3.38 (-2.92%). CEO: Tim Cook. Founded: April 1, 1976, Cupertino, California, United States. Headquarters: Cupertino, California, United States.';
var doc1 = new Document(w3c, 'A4');

// Print a normal document
printer.toPrint(doc1);

/*
// Print a blank document
printer.toPrint();
*/

/*
// Print a part of document
printer.toPrint(doc1, 2, 4);
*/

/*
// Print an unknown format document
var doc2 = new Document(apple, 'A4000');
printer.toPrint(doc2);
*/

/*
// Print a big document
printer.toPrint(doc1);
printer.toPrint(doc1);
printer.toPrint(doc1);
printer.toPrint(doc1);
printer.toPrint(doc1);
*/

/*
// Add an unknown format cartridge
printer.addCartridge(new Cartridge('007'));
*/

/*
// Add an empty
printer.addCartridge();
*/

var testCartridge = new Cartridge('YW42');

// Refill normal value
testCartridge.refillInk(10);

/*
// Refill zero
testCartridge.refillInk(0);
*/

/*
// Refill minus value
testCartridge.refillInk(-5);
*/

/*
// Refill text
testCartridge.refillInk('blabla');
*/

/*
// Refill nothing
testCartridge.refillInk();
*/