/**
 * Class PrintingMachine
 * @param {string} name - The name of printing machine
 */
function PrintingMachine(name) {
    var PRINTER_CODES = ['BK40', 'PK41', 'YW42', 'BE43'];
    var PRINTER_CARTRIDGES = {};
    var PAPER_TYPES = { 'A4': 'A4', 'LTR': 'LTR', 'B5': 'B5', 'A5': 'A5' };

    this.name = name;

    // Show cartridges a printing machine have
    this.getCartridges = function () {
        var result = [];
        if(Object.keys(PRINTER_CARTRIDGES).length === 0) {
            throw new Error('There is no printer cartridges')
        }
        for (var entry in PRINTER_CARTRIDGES) {
            if(PRINTER_CARTRIDGES.hasOwnProperty(entry)) {
                result.push(entry);
            }
        }
        console.info('Inks list', result);
        return result;
    };

    // Add cartridges to printing machine
    this.addCartridge = function (cartridge) {
        if(typeof (cartridge) != 'object') {
            throw new Error('Need to specify the type of cartridge')
        }
        if(Object.keys(cartridge).length === 0) {
            throw new Error('Can not add an empty cartridge')
        }

        var code = cartridge.getCode();

        if(PRINTER_CODES.indexOf(code) == -1) {
            throw new Error('The printer does not support this type of cartridge')
        }
        PRINTER_CODES.forEach(function (entry) {
            if(code == entry) {
                if(PRINTER_CARTRIDGES[code]) {
                    throw new Error('A cartridge already exists')
                }
                PRINTER_CARTRIDGES[code] = cartridge;
                console.info('New cartridge was added', PRINTER_CARTRIDGES[code].getCode());
            }
        })
    };

    // Remove the cartridge from the printing machine
    this.removeCartridge = function (cartridge) {
        if(typeof (cartridge) != 'object') {
            throw new Error('Need to specify the type of cartridge')
        }
        if(Object.keys(cartridge).length === 0) {
            throw new Error('Can not remove cartridge. No printers cartridges')
        }

        var code = cartridge.getCode();

        if(PRINTER_CODES.indexOf(code) == -1) {
            throw new Error('Can not remove cartridge. Container is empty')
        }
        PRINTER_CODES.forEach(function (entry) {
            if(code == entry) {
                if(!PRINTER_CARTRIDGES[code]) {
                    throw new Error('Can not remove cartridge. Container is empty')
                }
                var log = PRINTER_CARTRIDGES[code].getCode();
                PRINTER_CARTRIDGES[code] = {};
                console.info('One cartridge was removed', log);
            }
        })
    };

    // function show status of ink
    this.checkInkStatus = function() {
        var str = 'Inks status ';
        for (var entry in PRINTER_CARTRIDGES) {
            if(PRINTER_CARTRIDGES.hasOwnProperty(entry)) {
                str += ' [' + PRINTER_CARTRIDGES[entry].getCode() + ' : ' + PRINTER_CARTRIDGES[entry].getValue() + ']';
            }
        }
        console.info(str);
    };

    // Print document
    this.toPrint = function (doc, firstPage, lastPage) {
        if(!doc) {
            throw new Error('Cannot print a blank document');
        }
        if(!PAPER_TYPES[doc.getFormat()]) {
            throw new Error('Printer does not support this paper type');
        }
        if(firstPage && lastPage) {
            printCustomPages(doc.getPages(), firstPage, lastPage);
            return
        }

        printAllPages(doc.getPages());
    };

    // Print all pages
    function printAllPages(document) {
        document.forEach(function (page) {
            useInks();
            console.log(page);
        });
    }

    // Print custom page
    function printCustomPages(document, begin, end) {
        for(var i = begin; i <= end; i++) {
            useInks();
            var page = document[i];
            console.log(page);
        }
    }

    // Pump inks from cartridges
    function useInks() {
        for (var entry in PRINTER_CARTRIDGES) {
            if(PRINTER_CARTRIDGES.hasOwnProperty(entry)) {
                try {
                    PRINTER_CARTRIDGES[entry].pumpInk(3);
                } catch(err) {
                    throw new Error('Ink ended. Please add ink')
                }
            }
        }
    }
}