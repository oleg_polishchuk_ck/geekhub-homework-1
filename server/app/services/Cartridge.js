/**
 * Class Cartridge
 * @param {string} code - Use one of this: 'BK40' (black), 'PK41' (pink),  'YW42 (yellow)' and 'BE43' (blue)
 */
function Cartridge(code) {

    var code = code || 'BK40';  // Black by default
    var value = 0;              // current value
    var VALUE_LIMIT = 50;       // limit 50 ml

    this.getValue = function () {
        return value;
    };

    this.getCode = function () {
        return code;
    };

    this.refillInk = function (val) {
        if(typeof(val) !== "number" || !val) {
            throw new Error('The value should be a number')
        }
        if(val <= 0) {
            throw new Error('The value can not be less than or equal to zero')
        }
        if(value + val > VALUE_LIMIT) {
            throw new Error('The value is too high. Try a little less')
        }
        value += val;
        //console.log('The ink was refilled. Cartridge', code, '.Current value', value)
    };

    this.pumpInk = function (val) {
        if(typeof(val) !== "number" || !val) {
            throw new Error('The value should be a number')
        }
        if(val <= 0) {
            throw new Error('The value can not be less than or equal to zero')
        }
        if(value - val < 0) {
            throw new Error('The value is too high. Try a little less')
        }
        value -= val;
        //console.log('The ink was pumped. Current value', value)
    }
}