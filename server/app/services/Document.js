/**
 * Class Document
 * @param {string} type - The type of paper
 * @param {string} str - The text of paper
 */
function Document(str, type) {
    var format = type;
    var pages = str.split('. ');

    this.getFormat = function () {
        return format;
    };

    this.getPages = function () {
        return pages;
    }
}